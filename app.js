// allows use of features for web applications
const express = require('express');
const mongoose = require('mongoose');

// save the express application as a constant named app
const app = express();
const port = 3000;

// used in handling json request bodies
app.use(express.json());

app.use(express.urlencoded({extended: true}))

app.listen(port, () => console.log(`You got served on port ${port}`));

// mongodb atlas
mongoose.connect('mongodb+srv://admin:admin123@cluster0.chfze.mongodb.net/to_do_list?retryWrites=true&w=majority',{
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
})

//local mondoDB
/*mongoose.connect('mongodb://localhost:27017/to_do_list', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
})*/

let db = mongoose.connection;

// if connection error encountered, print in console
db.on('error',(console.error.bind(console,'connection error')))

// once connected, print success message in console
db.once('open', () => console.log("We're connected to our cloud database."))


//Define a task schema/blueprint
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'pending'
	}
})

/*sample task document
{
	nameL 'eat',
	status:'pending'
}*/

const userSchema = new mongoose.Schema({
	username: String,
	email: String,
	password: String,
	//tasks is an array of emmbedded/nested task objects
	tasks: [taskSchema]
})
// in the context of mongoose, a model is a class used in constructing documents
// the singular form and capital
const Task = mongoose.model('Task', taskSchema);
const User = mongoose.model('User', userSchema);

// api/routes
app.get('/', (req, res) => {
	res.send('hello world');
});

// create a user
app.post('/users', (req, res) => {
	// check for duplicate username or email
	User.find({$or: [
			{username: req.body.username},
			{email: req.body.email}
		]
	}, (findErr, duplicates) => {
		if (findErr) {
			console.error(findErr);
		}

		if(duplicates.length > 0){
			return res.status(403).json({
				message: "Duplicates found, kindly choose a different username and/or email."
			})
		} else {
					let newUser = new User({
				username: req.body.username,
				email: req.body.email,
				password: req.body.password,
				task: []
			})

			newUser.save((saveErr, newUser) => {
				if(saveErr) {
					return console.error(saveErr);
				}
					return res.status(201).json({
						message: `User ${newUser.username} successfully registered.`,
						data: {
							username: newUser.username,
							email: newUser.email,
							link_to_self: `/users/${newUser._id}`
						}
				})
			})
		}
	})
})

// show particular user
app.get('/users/:id', (req, res) => {
	User.findById(req.params.id, (err, user) =>{
		if (err) {
			return console.error(err)
		}

		return res.status(200).json({
			message: "User retrieved successfully",
			data: {
				username: user.username,
				email: user.email,
				tasks: `/users/${user._id}/tasks`
			}
		})
	})
})

// create a task for a particular user
/*app.post('/users/:userId/tasks', (req, res) => {
	User.findById(req.params.userId, (findErr, user) => {
		if (findErr) {
			return console.error(findErr);
		}; 
		if (user.tasks === 0){
			user.tasks.push({
				name: req.body.name
			});

			user.save((saveErr, modifiedUser) => {
				if (saveErr) {
					return console.error(saveErr);
				};

				return res.status(200).json({
					message: `${user.tasks[0].name} added task list of ${user.username}`,
					data: modifiedUser.tasks[0]
				})
			})
		} else {
			let duplicates = users.tasks.filter(task => 
				return task.name.toLowerCase() === req.body.name.toLowerCase()
			)

			if(duplicates.length > 0){
				return res.status(403).json({
					message: `${req.body.name} is already registered as a task.`
				});
			} else {
				user.tasks.push({
					name: req.body.name
				})

				user.save((saveErr, modifiedUser) => {
					if (saveErr) {
						return console.error(saveErr)
					}
					return res.status(200).json({
						message: `${modifiedUser.tasks[modifiedUser.tasks.length-1].name}
						added to task list of ${user.username}`,
						dsata: modifiedUser.tasks[modifiedUser.tasks.length-1] 
					})
				})

			}
		}
	});
});*/

app.post('/users/:userId/tasks', (req, res) => {
	User.findById(req.params.userId, (findErr, user) => {
if (findErr) {
return console.error(findErr);
};

if(user.tasks.length === 0){user.tasks.push({
		name: req.body.name
	});

user.save((saveErr, modifiedUser) => {
if (saveErr) {
return console.error(saveErr);
};

return res.status(200).json({
message: `${user.tasks[0].name} added to task list of ${user.username}`,
data: modifiedUser.tasks[0]
})
})
} else {
let duplicates = user.tasks.filter(task =>
task.name.toLowerCase() === req.body.name.toLowerCase()
);

if(duplicates.length > 0){
return res.status(403).json({
message: `${req.body.name} is already registered as a task.`
});
} else {
user.tasks.push({
name: req.body.name
})

user.save((saveErr, modifiedUser) => {
if (saveErr) {
return console.error(saveErr)
}

return res.status(200).json({
message: `${modifiedUser.tasks[modifiedUser.tasks.length-1].name} added to task list of ${user.username}`,
data: modifiedUser.tasks[modifiedUser.tasks.length-1]
})
})
}
}
});
});

// get details of a specific task of a user
app.get('/users/:userId/tasks/:taskId', (req, res) => {
	User.findById(req.params.userId, (findErr, user) => {

		if(findErr) {
			return console.error(findErr);
		}

		let task = user.tasks.id(req.params.taskId);

		if(task === null){
			return res.status(403).json({
				message: `Task with id ${req.params.taskId} cannot be found`
			})
		}

		if(user.tasks.length > 0){
			return res.status(200).json({
				message: `Tasks of ${user.username} retrieved successfully.`,
				data: user.tasks
			})
		} else {
			return res.status(200).json({
				message: `${user.username} currently has no tasks`
			})
		}
	})
})

data: user.tasks.id(req.params.taskId)
// data: task

// solution
// getting all task for a user
app.get('/users/:userId/tasks', (req, res) => {
		//retrieve user who owns tasks to be viewed
		User.findById(req.params.userId, (err, user) => {
			if(err) {
				return console.error(err);
			}
			//let tasks = user.tasks;
			if(user.tasks.length > 0){
				return res.status(200).json({
					message: `Tasks of ${user.username} retrieved successfully.`,
					data: user.tasks.id(req.params.taskId)
				})
			}else{
				return res.status(200).json({
					message: `${user.username} currently has no tasks.`
				})
			}
		})
	})

// 2. Implement a route for updating a task.

// Create the route for updating a task
	app.put('/users/:userId/tasks/:taskId', (req, res) => {
		
		// Find the user to update
		User.findById(req.params.userId, (findErr, user) => {
			
			// Add an if statement to catch any errors
			if(findErr) {
				return console.error(findErr);
			}
			// Find the task to update
			let task = user.tasks.id(req.params.taskId);

			// If the task does not exist return an error
			if(task === null){
				return res.status(403).json({
					message: `Task with id ${req.params.taskId} cannot be found`
				})

			// Else update the task and save to the database 
			}else{
				if((typeof req.body.name === "string" && req.body.name !== "") && (typeof req.body.status === "string" && req.body.status !== "")){
					task.name = req.body.name;
					task.status = req.body.status;
					user.save((saveErr, modifiedUser) => {
						if(saveErr) return console.error(saveErr);
						return res.status(200).json({
							message: `Task with id ${req.params.taskId} updated successfully.`,
							data: task
						})
					}) 
				}else{
					return res.status(403).json({
						message: "Task name and task status are both required fields."
					})
				}
			}
		})
	})

// 3. Implement a route for deleting a task.
	// 1. Create the route for updating a task ('/users/:userId/tasks/:taskId')
	app.delete('/users/:userId/tasks/:taskId', (req, res) => {
		
		// 2. Find the user to update
		User.findById(req.params.userId, (findErr, user) => {
			
			// 3. Add an if statement to catch any errors
			if(findErr) {
				return console.error(findErr);
			}

			// 4. Find the task to delete
			// 5. Delete the task (.remove())
			user.tasks.id(req.params.taskId).remove();

			// 6. Save to the database
			user.save((saveErr, modifiedUser) => {
				if(saveErr) return console.error(saveErr);
				return res.status(200).json({
					message: `Task with id ${req.params.taskId} deleted successfully.`,
					data: modifiedUser.tasks
				})
			})
		})
	})
